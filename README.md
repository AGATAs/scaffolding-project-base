npm start
- lance un serveur d'édition en local, en branchant allData de l'app sur le dossier courant.
- e



archi :

dossier projet
- package.json
- layers
- config.yml (avec en commentaire l'endroit ou trouver la conf par défaut)
... (contenu de allData)

edit-srv
- expose une api auprès de la quelle on peut :
  * s'autentifier (passwordless)
  * uploader des fichiers arbitraire 
- ajoute les données correspondante dans une branche au nom de l'utilisateur, depuis le commit indiqué via l'api à l'upload
- compile et test l'app avec les nouvelles données, si ok, merge dans master.
- pour chaque commit dans master, push les données sur le source du dossier projet (backup maitre) et déploie aux adresses listé des version mise à jour de l'app de consultation (en espérant avoir la bonne clef ssh pour faire tout ça)


frontend
- affiche la visu dans le navigateur en fonction des données fournies.



